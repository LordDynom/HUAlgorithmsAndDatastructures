/* --------------------------- */
// Niels de Waal
// 1698041
// Jorn Bunk
// Week 1 opdracht 1
/* --------------------------- */
#include <stdio.h>

int main()
{
	int x, y, z;

	while(!scanf("%d",&x)) {
		printf("Invalid input\n");
		while(getchar() != '\n') {
			;
		}
		printf("Voer getal in\n");
	}

	while(!scanf("%d",&y)) {
		printf("Invalid input\n");
		while(getchar() != '\n') {
			;
		}
		printf("Voer getal in\n");
	}

	z = x+y;

	printf("%d\n", z);
	return 0;
}
