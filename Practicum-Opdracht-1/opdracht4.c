/* --------------------------- */
// Niels de Waal
// 1698041
// Jorn Bunk
// Week 1 opdracht 4
/* --------------------------- */
#include <stdio.h>

int main() {
	int input;
	int offset;

	printf("Geef input ");
	scanf("%d", &input);

	if ((input * 10) > 100) {
		offset = 3;
	} else {
		offset = 2;
	}

	for(int i = 1; i <= 10; i++) {
		printf("[%*d]\n", offset, input * i);
	}

	return 0;
}
