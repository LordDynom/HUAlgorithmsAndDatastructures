/* --------------------------- */
// Niels de Waal
// 1698041
// Jorn Bunk
// Week 1 opdracht 3
/* --------------------------- */
#include <stdio.h>

int main() {
	int numberOfRows = 0;
	int rowNumber = 0;
	int counter = 0;

	printf("Enter the number of rows: ");
	scanf("%d",&numberOfRows);

	if (numberOfRows < 0 || numberOfRows > 80) {
		printf("Invalid input\n");
		return -1;
	}

	for (rowNumber = 0; rowNumber <= numberOfRows; rowNumber++) {
		for (counter = 0; counter < rowNumber; counter++) {
			printf("*");
		}
		 
		printf("\n");
	}
	for (rowNumber = numberOfRows - 1; rowNumber >= 0; rowNumber--) {
		for (counter = 0; counter < rowNumber; counter++) {
			printf("*");
		}
		printf("\n");
	}

	printf("\n");

	counter = 0;
	int i = 0;
	while (counter < numberOfRows) {
		while(i <= counter) {
			printf("*");
			i++;
		}
		i = 0;
		printf("\n");
		counter++;
	}

	counter = numberOfRows - 2;
	int j = 0;
	while (counter >= 0) {
		while(j <= counter) {
			printf("*");
			j++;
		}
		j = 0;
		printf("\n");
		counter--;
	}

	return 0;
}
