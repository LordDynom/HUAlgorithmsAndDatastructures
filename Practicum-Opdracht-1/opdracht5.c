/* --------------------------- */
// Niels de Waal
// 1698041
// Jorn Bunk
// Week 1 opdracht 5
/* --------------------------- */
#include <stdio.h>
#include <stdlib.h>

int main() {
	int i = 0, ch;
	char input[50] = "";
	char *leftover = NULL;

	while(((ch = getchar() ) != '\n' ) && (ch != ' ') && (i < 50)) {
		input[i++] = ch;
	}
	//input[50] = '\0';

	i = abs(strtol(input, &leftover, 0));

	/*
	if (input[0] == "-" || input[0] == "+") {
		printf("Break point hit");
		i = atoi(input);
		i = abs(i);
	} else {
		for (int j = 0; j <= 50; j++) {
			printf("%c", input[j]);
		}
	}
	*/

	printf("%i\n", i);

	return 0;
}
