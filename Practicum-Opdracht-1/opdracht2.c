/* --------------------------- */
// Niels de Waal
// 1698041
// Jorn Bunk
// Week 1 opdracht 2
/* --------------------------- */
#include <stdio.h>
#include <ctype.h>

int main() {
	int input = getchar();

	if (isupper(input)) {
		printf("%x\n", input);
	}

	if (islower(input)) {
		printf("%d\n", input);
	}

	if (!(isalpha(input))) {
		printf("%c\n", input);
	}

	return 0;
}
