//$Id$

/**
* @file stack.c
* @author Niels de Waal (1698041)
* @brief Code voor practicum opdracht 1 week 5
* @date 17-3-2017
* Jorn Bunk
*/
#include <stdio.h>
#include <assert.h>
#include "stack.h"

/**
 * @brief init_stack functie voor opzetten stack.
 *
 * Stack hoogte word gezet op -1 voor minimale hoogte.
 *
 * @param[in] ps Op te zetten stack.
 */
void init_stack(Stack* ps) {
	ps->top = -1;
}

/**
 * @brief Functie voor het zetten van nieuwe data op de stack.
 *
 * Nieuwe stack waarde word boven op de stack gezet.
 * Hoogte stack word aangepast om nieuwe waarde toegangkelijk te maken.
 *
 * @param[in] ps Aan te passen stack.
 * @param[in] data Op de stack te zetten data.
 */
void push(Stack* ps, int data) {
	assert(ps->top < STACKSIZE-1);
	(ps->top)++;
	ps->a[ps->top] = data;
}

/**
 * @brief Functie voor het ophalen van de laatste stack waarde.
 *
 * Data wordt opgehaald van bovenop de stack en lengte word verlaagt.
 *
 * @param[in] ps Aan te passen stack.
 *
 * @return De data van boven op de stack.
 */
int pop(Stack* ps) {
	assert(ps->top > -1);
	int data = ps->a[ps->top];
	ps->top -= 1;
	return data;
}

/**
 * @brief Functie voor het uitprinten van de stack.
 *
 * Data op de stack word uitgeprint.
 *
 * @param[in] s Uit te printen stack.
 */
void show(Stack s) {
	int i = 0;
	if (s.top > -1) {
		printf("%d", (s.a)[s.top]);
		for (i = (s.top)-1; i > -1; i--) {
			printf("->%d", s.a[i]);
		}
		putchar('\n');
	} else {
		puts("empty stack");
	}
}
