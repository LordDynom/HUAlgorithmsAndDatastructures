//$Id$

/**
* @file opdracht2.c
* @author Niels de Waal (1698041)
* @brief Code voor practicum opdracht 2 week 5
* @date 17-3-2017
* Jorn Bunk
*/
#include <stdio.h>
#include "stack.h"

/**
 * @brief Validator van expressie d.m.v een stack
 *
 * Open gaan de expressie karakters worden op de stack gezet.
 * Sluitende worden vergeleken met de stack. Als dit naar verwachte
 * waarden is dan word de expressie valide bevonden.
 *
 * @param[in] s Dit is de stack die gebruikt word voor de validatie.
 * @param[in] data[] De array met de te valideren expressie.
 * @param[in] size Lengte van de te valideren expressie.
 *
 * @return 0 bij goede uitkomst, 1 bij foutive.
 */
int expression_validator(Stack s, char data[], int size) {
	int i = 0;
	for (i = 0; i < size; i++) {
		if (data[i] == 0x28 || data[i] == 0x3C || data[i] == 0x5B || data[i] == 0x7B) {
			push(&s, data[i]);
		} else if (data[i] == 0x29 || data[i] == 0x3E || data[i] == 0x5C || data[i] == 0x7C) {
			if (data[i] == 0x29) {
				if (s.a[s.top] == 0x28) {
					pop(&s);
					continue;
				} else {
					return 1;
				}
			} else if (data[i] == (s.a[s.top] + 0x02)) {
				pop(&s);
				continue;
			} else {
				return 1;
			}
		}
	}
	if (s.top != 0) {
		return 1;
	}
	return 0;
}

/**
 * @brief main functie.
 *
 * Te valideren expressie en stack worden aangemaakt.
 * Waarde over validiteit van expressie word geprint.
 */
int main(void) {
	char data[8] = "<<()>>{}";
	Stack stack = {{0}, -1};
	
	int result = expression_validator(stack, data, 8);

	printf("%d\n", result);
}	
