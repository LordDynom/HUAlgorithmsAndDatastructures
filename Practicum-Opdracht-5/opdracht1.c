//$Id$

/**
 * @file opdracht1.c
 * @author Niels de Waal (1698041)
 * @brief Code voor practicum opdracht 1 week 5
 * @date 17-3-2017
 * Jorn Bunk
 */

#include <stdio.h>
#include <stdbool.h>
#include <assert.h>

/**
 * @brief recusieve functie voor bepalen gesorteerdheid van array.
 *
 * If functie kijkt of twee waarden gesorteerd zijn.
 * Dezelfde functie word hierna opnieuw aangeroepen voor de volgnede waarden.
 *
 * @param[in] a[] array die check ondergaat.
 * @param[in] size lengte van array
 *
 * @return zelfde functie voor volgende waardes.
 */
bool is_sorted(int a[], int size) {
	assert(size > 0);

	if (size == 2) {
		return true;
	} else if (a[size - 1] > a[size - 2]) {
		return false;
	}

	return is_sorted(a, size-1);
}

/**
 * @brief main functie
 *
 * Array word aangemaakt, en gecheckt.
 */
int main(void) {
	int array[10] = {4,7,24,7,9,6,5,2,4,7};
	int sorted = is_sorted(array, 10);

	printf("%d\n", sorted);

	return 0;
}
