//$Id$

/**
* @file main.c
* @author Niels de Waal (1698041)
* @brief Code voor practicum opdracht 3 week 5
* @date 17-3-2017
* Jorn Bunk
*/
#include <stdio.h>
#include "queue.h"

/**
 * @brief main functie voor opdracht 3
 *
 * Array word opgezet met in de queue te zetten waarden.
 * Deze worden in de queue gezet. Queue word geprint en geleegd.
 */
int main(void) {
	int array_to_be_queued[10] = {0,1,2,3,4,5,6,7,8,9};
	int i = 0;
	Queue queue;

	init_queue(&queue);

	for (i = 0; i < 10; i++) {
		enqueue(&queue, array_to_be_queued[i]);
	}

	show(queue);

	for (i = 0; i < 10; i++) {
		printf("%d\n", dequeue(&queue));
	}
}
