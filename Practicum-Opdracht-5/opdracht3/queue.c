//$Id$

/**
* @file queue.c
* @author Niels de Waal (1698041)
* @brief Code voor practicum opdracht 3 week 5
* @date 17-3-2017
* Jorn Bunk
*/
#include <stdio.h>
#include "queue.h"

/**
 * @brief Initialiseer functie voor de queue.
 *
 * Queue word geïnitialiseerd d.m.v het aanmaken van de head, tail en size waarde.
 * 
 * @param[in] pq Pointer naar de te initialiseren queue struct.
 */
void init_queue(Queue* pq) {
	pq->head = NULL;
	pq->tail = NULL;
	pq->size = 0;
}

/**
 * @brief Functie voor het toevoegen van een waarde aan de queue.
 *
 * Waarde word toegevoed aan de queue.
 * Size van queue word verhoogd om lengte van de queue naar de nieuwe waarde aan te passen.
 * 
 * @param[in] pq Pointer naar queue.
 * @param[in] data Data die aan de queue moet worden toegevoegd.
 */
void enqueue(Queue* pq, int data) {
		pq->a[pq->size] = data;
		pq->tail = data;
		pq->size++;
}

/**
 * @brief Eerste waarde in de queue wordt weggehaald en gereturned.
 *
 * Eerste queue waarde word aan de data variabele gegeven.
 * De for loop schuift alle waarden in de queue 1 positie op.
 * De head word aangepast zodat deze de nieuwe eerste waarde bevat.
 * 
 * @param[in] pq Pointer naar queue.
 *
 * @return data
 */
int dequeue(Queue* pq) {
	int data = pq->head;
	int i = 0;
	for(i = 0; i < pq->size; i++) {
		pq->a[i] = pq->a[i+1];
	}
	pq->head = pq->a[0];
	pq->size--;
	pq->tail = pq->a[pq->size];
	
	return data;
}

/**
 * @brief Functie die inhoud van queue print.
 *
 * Functie gaat alle waarden binnen de queue af en print ze.
 * 
 * @param[in] p Pointer naar queue.
 */
void show(Queue q) {
	int i = 0;
	if(q.size > 0) {
		printf("%d", (q.a)[q.size]);
		for (i = 1; i < q.size; i++) {
			printf("->%d", q.a[i]);
		}
		printf("\n");
	} else {
		puts("empty queue");
	}
}
