/* --------------------------- */
// Niels de Waal
// 1698041
// Jorn Bunk
// Week 2 opdracht 3
/* --------------------------- */
#include <stdio.h>
#include <string.h>

//$Id$

/**
 * @file opdracht3.c
 *
 * @brief Bestand met code voor practicum opdracht 3
 *
 */

/**
 * @brief Verwisseld het aangegeven onderdeel uit een array met zijn tegenovergestelde plek
 *
 * Het aangegeven character uit de input array word omgewisseld met de waarde die 
 * op de zelfde locatie ligt maar dan aan de achterkant. BIj deze functie wordt 
 * gebruik gemaakt van indices.
 *
 * https://cloud.githubusercontent.com/assets/5456665/21447433/bc21ad60-c8fe-11e6-9154-b3b682cad607.png
 *
 * @param swapArray Dit is een pointer naar het array waarin 2 eenheden verwisseld moeten worden
 * @param i De index waarop de te vervangen eenheid staat
 * @param max De lengte van de string die in de array staat
 *
 */
void swapIndice(char swapArray[], int i, int max) {	
	swapArray[i] -= swapArray[max-i] = (swapArray[i] += swapArray[max-i]) - swapArray[max-i];
}

/**
 * @brief Verwisseld het aangegeven onderdeel uit een array met zijn tegenovergestelde plek
 *
 * Het aangegeven character uit de input array word omgewisseld met de waarde die 
 * op de zelfde locatie ligt maar dan aan de achterkant. BIj deze functie wordt gebruik
 * gemaakt van pointers die naar de waarden in het array wijzen.
 * 
 * https://cloud.githubusercontent.com/assets/5456665/21447433/bc21ad60-c8fe-11e6-9154-b3b682cad607.png
 *
 * @param swapArray Dit is een pointer naar het array waarin 2 eenheden verwisseld moeten worden
 * @param i De index waarop de te vervangen eenheid staat
 * @param max De lengte van de string die in de array staat
 */
void swapPointer(char swapArray[], int i, int max) {
	char *p = swapArray;	
	*(p+i) -= *(p+(max-i)) = (*(p+i) += *(p+(max-i))) - *(p+(max-i));
}

/**
 * @brief Main functie die aantoont dat de swap functies werken
 *
 * Eerst worden er 2 charater array's aangemaakt. De eerste word eerst gevult met de input van de user. 
 * Deze word hierna gekopiëerd naar de tweede (omdat hier niet gebruikt word gemaakt van een buffer array.
 * De eerste for loop demonstreerd de werking van de swapIndice functie. De loop loopt tot 
 * hij bij het midden is. Zo worden alle characters verwisseld met hun tegenovergestelde positie.
 * De tweede loop demonstreerd de werking van de swapPointer functie. Deze loop werkt het zelfde
 * als de eerste.
 * Als laatste worden de twee array's geprint, zo word aangetoond dat de functie's inderdaad de
 * input spiegelen.
 */
int main() {
	char swap[80] = "";
	char swap2[80] = "";

	fgets(swap, 80, stdin);
	int max = strlen(swap) - 2;

	memcpy(swap2, swap, sizeof(swap2));

	for (int i = 0; (i + 1) <= max - i; i++) {
		swapIndice(swap, i, max);
	}

	for (int j = 0; (j + 1) <= max - j; j++) {
		swapPointer(swap2, j, max);
	}

	printf("%s\n", swap);
	printf("%s\n", swap2);

	return 0;
}
	
