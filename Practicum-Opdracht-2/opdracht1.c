/* --------------------------- */
// Niels de Waal
// 1698041
// Jorn Bunk
// Week 2 opdracht 1
/* --------------------------- */
#include <stdio.h>
#include <time.h>

//$Id$
/**
 * @file opdracht1.c
 *
 * @brief Bestand met code voor practicum opdracht 1
 *
 */

/**
 * @brief Main functie
 *
 * Eerst word er een integer array voor 10 getallen aangemaakt. 
 * Daarna een couter variable. Als laatste word de srand functie geïnitialiseerd.
 *
 * De for loop zorgt dat alle plekken in het array een waarde van 1 of 0 hebben.
 * Dit door van een random getal de modulus te nemen.
 *
 * De tweede for loop telt het aantal 1'en in het array.
 *
 * Als het aantal 1'en gelijk is aan 5 betekend dit dat de hoeveelheid 1'en en 0'en even is.
 */
int main() {
	int mainArray[10] = {};
	int count = 0; 
	srand(time(NULL)); 

	for (int i = 0; i < 10; i++) {
		mainArray[i] = rand() % 2;
	}

	for (int j = 0; j < 10; j++) {
		if (mainArray[j]) {
			count++;
		} else {
			continue;
		}
	}
	
	if (count == 5) {
		printf("Result is even\n");
	} else {
		printf("Result is uneven\n");
	}

	return 0;
}
