/* --------------------------- */
// Niels de Waal
// 1698041
// Jorn Bunk
// Week 2 opdracht 5
/* --------------------------- */
#include <stdio.h>

// $Id$

/**
 * @file opdracht5.c
 *
 * @brief Bestand met code voor practicum opdracht 5
 */

/**
 * @brief Main functie voor uitprinten data volgens gegeven patroon
 *
 * De twee for loops lopen door alle eenheden in de matrix. 
 * Als er het huidige getal -1 is word deze vervangen door een -.
 *
 * @return 0
 */
int main() {
	/// Matrix met uit te printen data
	int matrix[][10] = {{-1,-1,-1,-1,-1,-1, 0, 1,-1, 1},
			    { 1,-1,-1,-1,-1,-1,-1,-1,-1, 1},
			    {-1,-1, 0,-1, 1,-1,-1,-1,-1,-1},
			    {-1,-1,-1,-1, 0,-1,-1,-1,-1,-1},
			    {-1,-1,-1,-1,-1,-1, 0,-1,-1, 1},
			    {-1,-1,-1,-1,-1, 1,-1,-1, 1,-1},
			    {-1,-1,-1,-1,-1,-1, 0,-1,-1, 0},
			    { 0,-1,-1,-1,-1,-1,-1,-1,-1,-1},
			    {-1,-1, 1, 1,-1,-1,-1,-1,-1, 1},
			    { 0,-1, 1,-1,-1,-1,-1,-1, 0,-1}};

	for (int i = 0; i < 10; i++) {
	       for (int j = 0; j < 10; j++) {	
		       if (matrix[i][j] == -1) {
			       printf("-");
		       } else {
			       printf("%d", matrix[i][j]);
		       }
		}
	       printf("\n");
	}
	       

	return 0;
}
