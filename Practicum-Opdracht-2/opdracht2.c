/* --------------------------- */
// Niels de Waal
// 1698041
// Jorn Bunk
// Week 2 opdracht 2
/* --------------------------- */
#include <stdio.h>
#include <string.h>

//$Id$

/**
 * @file opdracht2.c
 *
 * @brief Bestand met code voor practicum opdracht 2
 *
 */

/**
 * @brief Functie voor het vergelijken van 2 strings
 *
 * Vergelijkt 2 string array's en bepaald vanaf welke plek 
 * de 2 strings verschillen.
 *
 * @param s1 Eerste string
 * @param s2 Tweede string
 *
 * @return De locatie van af waar de 2 strings verschillend van elkaar zijn
 */

int compareString(char *s1, char *s2) {
	int count = 0;
	char* p1 = s1;
	char* p2 = s2;
	for (int i = 0; i < strlen(s1)+1; i++) {
		if (s1[i] == s2[i]) {
			count++;
			continue;
		} else {
			return i;
			break;
		}
	}
}

/**
 * @brief Main fucntie voor input van te vergelijken strings.
 *
 * Eerst worden er 2 character array's aangemaakt. Hierna worden deze gevult naar de input van de gebruiker.
 * Het resultaat wordt eerst gevormt in de rusult variable, en hierna geprint.
 */
int main() {
	char firstString[80];
	char secondString[80];

	printf("Enter first string: ");
	fgets(firstString, 80, stdin);

	printf("Enter second string: ");
	fgets(secondString, 80, stdin);

	int result = compareString(firstString, secondString);

	printf("%d\n", result);

	return 0;
}


