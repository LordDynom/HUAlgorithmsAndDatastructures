/* --------------------------- */
// Niels de Waal
// 1698041
// Jorn Bunk
// Week 2 opdracht 4
/* --------------------------- */
#include <stdio.h>
#include <stdlib.h>

// $Id$

/**
 * @file opdracht4.c
 *
 * @brief Bestand met code voor practicum opdracht 4
 */

typedef struct voorwerp
{
	int nummer; /**< Het nummer wat vronden is aan het voorwerp */
	char naam[20]; /**< De naam van het voorwerp */
	double gewicht; /**< Het gewicht van het voorwerp */
	double lengte; /**< De lengte van het voorwerp */
} VOORWERP; /**< Struct voor het definiëren van een voorwerp */

/**
 * @brief Main functie voor het inlezen van de data in de struct en deze netjes te printen
 *
 * Als eerste word de structure aangemaakt. Hierna wordt door middel van vragen
 * de structure ingevult.
 */
int main() {
	VOORWERP vwp;

	printf("nummer: ");
	scanf("%d", &vwp.nummer);

	printf("naam: ");
	scanf("%s", &vwp.naam);

	printf("gewicht: ");
	scanf("%lf", &vwp.gewicht);

	printf("lengte: ");
	scanf("%lf", &vwp.lengte);

	printf("%s heeft nummber %d, weegt %lf kg en is %lf cm\n", vwp.naam, vwp.nummer, vwp.gewicht, vwp.lengte);

	return 0;
}
