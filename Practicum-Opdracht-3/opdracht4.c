/* --------------------------- */
// Niels de Waal
// 1698041
// Jorn Bunk
// Week 3 opdracht 4
/* --------------------------- */
#include <stdio.h>

/**
 * def XORSWAP(a, b)
 * Macro voor een XOR swap, deze verwisseld twee elementen zonder tussenkomst van een buffer
 */
#define XORSWAP(a, b)	((a)^=(b),(b)^=(a),(a)^=(b))

//$Id$

/**
 * @file opdracht4.c
 * 
 * @brief Code voor practicum opdracht 4.
 */

/**
 * @brief functie voor het spiegelen van de matrix t.o.v de hoofddiagonaal.
 *
 * For loop loopt over alle waarden behalve de laatste.
 * De tweede for loop gaat over de waarde die in het array van de tweedimensionale array zitten.
 * Elke waarde word verwisseld met de tegenoverstelde locatie.
 * (Deze aanpak werkt alleen bij een vierkante matrix).
 *
 * @param[in] size Lengte van de as van de matrix.
 * @param[in] matrix De matrix die gespiegeld moet worden
 */
void transpose_matrix(int size, int matrix[][size]) {
	for (int i = 0; i <= size -2; i++) {
		for (int j = i + 1; j <= size -1; j++) {
			XORSWAP(matrix[i][j], matrix[j][i]);
		}
	}
}

/**
 * @brief main functie
 *
 * Eerst word de te spiegelen matrix aangemaakt.
 * Hierna word hij gespiegeld en door de for loop geprint.
 */
int main(void) {
	int matrix[3][3] = {{1,1,1},
			    {2,2,2},
			    {3,3,3}};

	transpose_matrix(3,matrix); 

	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			printf("%d\t", matrix[i][j]);
		}
		printf("\n");
	}

	return 0;
}
