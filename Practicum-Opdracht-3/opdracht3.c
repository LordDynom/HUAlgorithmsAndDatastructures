/* --------------------------- */
// Niels de Waal
// 1698041
// Jorn Bunk
// Week 3 opdracht 3
/* --------------------------- */
#include <stdio.h>
#include <stdbool.h>

//$Id$

/**
 * @file opdracht3.c
 *
 * @brief Code voor practicum opdracht 3
 */

/**
 * @brief fucntie die kijkt of array aan bepaalde eisen voldoet.
 *
 * De input array word gecontroleerd op:
 * - De aanwezigheid van alleen 1'en of 0'en, 
 * - De gelijkheid van het aantal 1'en en 0'en,
 * - Of een waarde niet 3 keer achter elkaar voorkomt in het array.
 *
 * De for loop kijkt bij de eerste if statement of het huidige element voldoet aan de eerste eis.
 * Het daarop volgende if statement telt aan de hand van de waarde het aantal 0'en.
 * Hierna wordt gekeken of de huidige waarde gelijk is aan de vorige.
 * Aan het eind van elke loop ronde word gekeken of er 3 keer de zelfe waarde aangetroffen is.
 * Het laatste if statement kijkt of het aantal 0'en gelijk is aan het aantal 1'en.
 *
 * @param[in] a Array die vergeleken moet worden.
 * @param[in] size Lengte van het te controleren.
 *
 * @return Boolean waarde of array aan de eisen voldoet.
 */
bool valid_row(int a[], int size) {
	int zero_count = 0;
	int tri_count = 0;

	for (int counter = 0; counter < size; counter++) {
		if (a[counter] == 1 || a[counter] == 0) {
			if (!a[counter]) {
				zero_count++;
			}
			
			if (a[counter] == a[counter-1]) {
				tri_count++;
			} else {
				tri_count = 0;
			}
		} else {
			return false;
			break;
		}
 
		if (tri_count == 3) {
			return false;
			break;
		}
	}

	if (zero_count != size/2) {
		return false;
	}

	return true;
}

/**
 * @brief main functie
 *
 * Eerst word het te controleren array aangemaakt.
 * Hierna word deze getest en het resultaat geprint.
 */
int main(void) {
	int array[10] = {0,1,1,0,0,1,1,0,0,1};

	bool result = valid_row(array, 10);

	printf("%d\n", result);

	return 0;
}
