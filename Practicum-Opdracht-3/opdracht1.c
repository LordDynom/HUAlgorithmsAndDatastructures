/* --------------------------- */
// Niels de Waal
// 1698041
// Jorn Bunk
// Week 3 opdracht 1
/* --------------------------- */
#include <stdio.h>

// $Id$
/**
 * @file opdracht1.c
 *
 * @brief Code voor practicum week 3 opdracht 1
 */

/**
 * @brief Functie voor tellen hoeveelheid van parameter n aanwezig is in een array
 *
 * Deze functie gebruikt een loop om door alle eenheden van het array te lopen.
 * Elke keer dat het huidige element gelijk is aan de input word de tel variabele verhoogd.
 *
 * @param[in] a Het array waar de te tellen elementen in staan
 * @param[in] size Het aantal elementen in het array
 * @param[in] n Welk element getelt moet worden.
 * 
 * @return count variabele met aantal aanwezige elementen n
 */
int count(int a[], int size, int n) {
	int count = 0;

	for (int i = 0; i < size; i++) {
		if (a[i] == n) {
			count++;
		} else {
			continue;
		}
	}

	return count;
}

/**
 * @brief Main functie met te tellen array
 *
 * Dit is de main functie. Deze bevat het array waaruit elementen geteld moeten worden.
 */
int main(void) {
	int array[9] = {1,2,3,4,1,2,6,8,9};

	printf("%d\n", count(array, 9, 2));

	return 0;
}
