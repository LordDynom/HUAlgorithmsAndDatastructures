/* --------------------------- */
// Niels de Waal
// 1698041
// Jorn Bunk
// Week 3 opdracht 2
/* --------------------------- */
#include <stdio.h>
#include <stdbool.h>

//$Id$

/**
 * @file opdracht2.c
 *
 * @brief Bestand met code voor practicum opdracht 2
 */

/**
 * @brief functie voor het vergelijken van twee arrays
 *
 * De for loop gaat over alle elementen. Zodra het een element tegen komt wat verschilt,
 * word de check false en stopt de loop.
 *
 * @param[in] a1 Array waarmee a2 vergeleken wordt
 * @param[in] a2 Array waarmee a1 vergeleken wordt
 * @param[in] size lengte van de beide array's
 *
 * @return Een bool die true is als de array's gelijk zijn.
 */
bool equal_rows(int a1[], int a2[], int size) {
	bool check = false;
	for (int i = 0; i < size; i++) {
		if (a1[i] == a2[i]) {
			check = true;
		} else {
			check = false;
			break;
		}
	}
	
	return check;
}

/**
 * @brief main functie voor vergelijking array's
 *
 * Eerst worden de twee te vergelijken array's aangemaakt.
 * De print statement geeft een 1 bij true en 0 bij false.
 */
int main(void) {
	int array1[10] = {1,2,3,4,5,1,2,3,4,5};
	int array2[10] = {1,2,3,4,5,1,2,3,4,5};
	
	printf("%d\n", equal_rows(array1, array2, 10));

	return 0;
}

