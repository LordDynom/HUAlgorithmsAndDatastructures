/* --------------------------- */
// Niels de Waal
// 1698041
// Jorn Bunk
// Week 3 opdracht 5
/* --------------------------- */
#include <stdio.h>
#include <math.h>

//$Id$

/**
 * @file opdracht5.c
 *
 * @brief Code voor opdracht 5 
 */

/**
 * @brief Functie die de waarde van bit n uit byte ch leest.
 *
 * Byte ch word ge'AND met 1 op locatie 7-n.
 * De bits worden getelt vanaf links dus moet 1 geshift worden 7-n plaatsen.
 *
 * @param ch Byte welke gecheckt wordt
 * @param n Te checken locatie, geteld vanaf links naar rechts.
 *
 * @return Het resultaat van de AND operatie.
 */
int get_bit(char ch, int n) {
	return ((ch) & (1<<(7-n)));
}

/**
 * @brief Functie die byte ch cyclish roteerd n keer roteerd.
 *
 * Byte ch wordt n keer geroteerd. Wanneer n groter is dan 0 wordt dit de linker 
 * kant op gedaan, recht als deze kleiner is dan nul. Voor de rotatie wordt 
 * gebruik gemaakt van de x86_64 rotatie instructie. Deze word door de for-loop
 * n keer uitgevoerd.
 *
 * @param ch Byte welke geroteerd word.
 * @param n Aantal uittevoeren rotaties.
 *
 * @return Resultaat van rotatie's ch.
 */
char verschuif_cyclisch(char ch, int n) {
	if (n > 0) {
		for (int i = 0; i < n; i++) {
			__asm__ ("rol %0"
				: "=r" (ch)
				: "r" (ch));
		}
	}
        if (n < 0) {
		for (int i = 0; i < abs(n); i++) {
			__asm__ ("ror %0"
				: "=r" (ch)
				: "r" (ch));
		}
	}
	return ch;
}

/**
 * @brief Main functie voor opdracht 5.
 *
 * Eerst worden de te bewerken variabelen aangemaakt.
 * De resultaten van de functies word geprint.
 */
int main(void) {
	char rotatie = 0x41;
	char bit =  0xEB;

	printf("Bit waarde 5 van 0xEB: %d\n", get_bit(bit, 5));
	printf("2 keer geroteerde waarde 0x41: 0x%02x\n", verschuif_cyclisch(rotatie, 2));

	return 0;
}
