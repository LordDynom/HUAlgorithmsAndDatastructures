//$Id$

#include <stdio.h>
#include <stdlib.h>

/**
 * @file opdracht4.c
 * @author Niels de Waal (1698041)
 * @brief Document met code voor practicum week 4 opdracht 4
 * @date 15-3-2017
 * Jorn Bunk
 */

/**
 * @brief Functie voor het uitlezen van een bestand en het filteren van de getallen
 *
 */
int getIntegers(char* filename, int* a, int size) {
	FILE* input_file;
	int byte_c;
	int negative_flag = 0;
	int end_number_flag = 0;
	int number = 0;
	int counter = 0;

	input_file = fopen(filename, "r");
	byte_c = fgetc(input_file);

	while (byte_c != EOF) {
		if (!(byte_c >= 0x30 && byte_c <= 0x39) && byte_c != 0x2d) {
			end_number_flag = 0;
			byte_c = fgetc(input_file);
			continue;
		} 
		if (byte_c == 0x2d) {
			negative_flag = 1;
			byte_c = fgetc(input_file);
		} 
		while (byte_c >= 0x30 && byte_c <= 0x39) {
			number *= 10;
			number += byte_c - 0x30;
			byte_c = fgetc(input_file);
		}
		if (negative_flag) {
			number *= -1;
			negative_flag = 0;
		}
		end_number_flag = 0;
		if(!end_number_flag) {
			a[counter] = number;
			number = 0;
		}
		counter++;
	}

	return counter;
}

int main(void) {
	int a[100];
	int n = getIntegers("getallen.txt", a, 100);

	int i;

	if (n > 0) {
		puts("gevonden getallen: ");

		for (i = 0; i < n; i++) {
			printf("%d ", a[i]);
		}

		putchar('\n');
	}

	return 0;
}
