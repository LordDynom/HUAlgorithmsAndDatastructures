//$Id$

#include <stdio.h>;

/**
 * @file opdracht5.c
 * @author Niels de Waal (1698041)
 * @brief Document met code voor practicum week 4 opdracht 4
 * @date 8-4-2017
 * Jorn Bunk
 */

/**
 * @brief Functie voor het bepalen van het langste woord in bestand.
 *
 * Bestand word geopend. Hierna word het uitgelezen byte voor byte.
 * Elke woord wordt gescheiden door een spatie. Als er een spatie voor komt 
 * wordt de lengte van het woord vergeleken met het langste, als dat langer is
 * dan word de lengte vervangen.
 *
 * @param[in] filename Bestandsnaam van uit te lezen bestand.
 *
 * @return max_length Lengte van het langste woord.
 */
int getMaxWordLength(char* filename){
	FILE* in_file;
	int byte_c;
	int counter;
	int max_length = 0;

	in_file = fopen(filename, "r");
	byte_c = fgetc(in_file);

	while(byte_c != EOF) {
		if(!isalnum(byte_c)) {
			if (counter > max_length) {
				max_length = counter;
			}
			counter = 0;
		} else {
			counter++;
		}

		byte_c = fgetc(in_file);
	}

	fclose(in_file);

	return max_length;
}

/**
 * @brief Functie voor het ophalen van woorden uit een bestand en deze worden in a[] gezet.
 *
 * Het bestand wordt geopend en uitgelezen door de scanf functie.
 * De hoeveelheid woorden wordt terug gegeven.
 *
 * @param[in] filename Bestandsnaam van te lezen bestand.
 * @param[in] a[] Array waar woorden in gezet worden.
 *
 * @return word_counter Hoeveelheid woorden.
 */
int getWords(char* filename, char a[][word_size]) {
	FILE* in_file;
	in_file = fopen(filename, "r");
	int word_counter = 1;
	int counter = 0;

	while (word_counter>0) {
		word_counter = fscanf(in_file, "%s", a[counter]);
		counter++;
	}

	word_counter = counter - 1;

	return word_counter;
}

/**
 * @brief main functie
 *
 * Hoofd fucntie voor opdracht 5.
 *
 * @return 0 bij goed verloop van functie.
 */
int main(void) {
	char filename[] = "opdracht_4_5.c";
	int word_size = getMaxWordLength(filename);

	char a[1000][word_size];
	int n = getWords(filename, a);

	if (n > 0) {
		puts("gevonden woorden:");
		int i = 0;
		for (i = 0; i < n; i++) {
			printf("%3d %s\n",i,a[i]);
		}
	}

	return 0;
}
